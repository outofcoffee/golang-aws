# AWS samples in Golang

A few AWS tools written in Golang.

# Pre-requisites

Install all dependencies:

    go get ./...

Either add an the ``~/.aws/credentials`` file, like this:

    [default]
    aws_access_key_id = AKID1234567890
    aws_secret_access_key = MY-SECRET-KEY

...or set these environment variables:

    AWS_ACCESS_KEY_ID=AKID1234567890
    AWS_SECRET_ACCESS_KEY=MY-SECRET-KEY

# Build & run

Set up ``GOPATH``:

	GOPATH="$(pwd)"

## List instances

Install:

	go install bitbucket.org/outofcoffee/golang-aws/instances
	
Run:

	./bin/instances

Example output:

	> Number of reservation sets:  2
	  > Number of instances:  1
	    - Instance ID:  i-a704ef6a
	  > Number of instances:  1
	    - Instance ID:  i-ed0f8950

## List security groups

Install:

	go install bitbucket.org/outofcoffee/golang-aws/securitygroups
	
Run:

	./bin/securitygroups

...filter by group name:

	./bin/securitygroups -groupName=demo-group

Example output:

	- Name:  demo-group
	  > Number of inbound IP permissions:  6
	    - IP protocol:  tcp
	    - From port:  8090
	    - To port:  8090
	    > Number of IP Ranges:  1
	      - CIDR IP:  18.123.42.48/32