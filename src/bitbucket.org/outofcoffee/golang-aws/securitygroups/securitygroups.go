package main

import (
    "fmt"
    "flag"

    "github.com/aws/aws-sdk-go/aws"
    "github.com/aws/aws-sdk-go/service/ec2"
)

var groupName string
func init() {
	flag.StringVar(&groupName, "groupName", "", "Filter by group name")
	flag.Parse()
}

func main() {
    // Create an EC2 service object in the "eu-west-1" region
    // Note that you can also configure your region globally by
    // exporting the AWS_REGION environment variable
    svc := ec2.New(&aws.Config{Region: aws.String("eu-west-1")})

    // Call the DescribeSecurityGroups Operation
    resp, err := svc.DescribeSecurityGroups(nil)
    if err != nil {
        panic(err)
    }
    
    // determine if groups are filtered
    var filtered = (len(groupName) > 0)

    if (!filtered) {
    	fmt.Println("> Number of security groups: ", len(resp.SecurityGroups))
    }
    
    // resp has all of the response data, pull out security groups:
    for idx, res := range resp.SecurityGroups {
    	if (!filtered || groupName == *res.GroupName) {
	    	fmt.Println("  - Name: ", *res.GroupName)
	    	
	        fmt.Println("  > Number of inbound IP permissions: ", len(res.IPPermissions))
	        for _, perm := range resp.SecurityGroups[idx].IPPermissions {
	            fmt.Println("    - IP protocol: ", *perm.IPProtocol)
	            
	            if (nil != perm.FromPort) {
	    			fmt.Println("    - From port: ", *perm.FromPort)
	    		}
	            if (nil != perm.ToPort) {
	    			fmt.Println("    - To port: ", *perm.ToPort)
	            }
	            
	            fmt.Println("    > Number of IP Ranges: ", len(perm.IPRanges))
	            for _, ipr := range perm.IPRanges {
	            	fmt.Println("      - CIDR IP: ", *ipr.CIDRIP)	
	            }
	            
	            fmt.Println("    ---")
	        }
        }
    }
}
